module patient.register {
    requires javafx.controls;

    exports no.ntnu.edu.idatt2001.williagt.mappe2.view;
    exports no.ntnu.edu.idatt2001.williagt.mappe2.model.patient;
    exports no.ntnu.edu.idatt2001.williagt.mappe2.model;
    exports no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements;

    opens no.ntnu.edu.idatt2001.williagt.mappe2.model to javafx.base;
    opens no.ntnu.edu.idatt2001.williagt.mappe2.model.patient to javafx.base;
}