package no.ntnu.edu.idatt2001.williagt.mappe2.controller;

import javafx.stage.Stage;
import no.ntnu.edu.idatt2001.williagt.mappe2.model.FileManager;
import no.ntnu.edu.idatt2001.williagt.mappe2.model.patient.Patient;
import no.ntnu.edu.idatt2001.williagt.mappe2.model.patient.PatientRegister;
import no.ntnu.edu.idatt2001.williagt.mappe2.view.PopUps;
import no.ntnu.edu.idatt2001.williagt.mappe2.view.UserInterface;

import java.io.File;

/**
 * Class responsible for communication between the classes in the view and model packages.
 */
public class PatientRegisterController {

    /**
     * Adds a patient to the register that is sent in and updates the ui that is sent in if this is successful.
     * If something goes awry, an error messages will be shown.
     *
     * @param ui the user interface that wants to perform the method
     * @param register the register that is manipulated in the method
     */
    public void addPatient(UserInterface ui, PatientRegister register){
        try{
            PopUps popUps = new PopUps();
            Patient newPatient = popUps.addOrEditPatient(true);
            if(register.addPatient(newPatient)){
                ui.updateObservablePatients();
            }
        }catch(IllegalArgumentException e){
            PopUps.errorPopUp(e.getMessage());
            addPatient(ui, register);
        }
    }

    /**
     * Edits a specified Patient from the TableView in the UserInterface that is sent in to the method. If no Patient is
     * selected, or something illegal is done with the Patient, an error message will be shown.
     *
     * @param ui the user interface that wants to perform the method
     */
    public void editPatient(UserInterface ui){
        Patient patientToBeEdited = ui.getPatientFromTable();

        if(patientToBeEdited == null){
            PopUps.errorPopUp("You did not choose a patient properly.");
            return;
        }
        try{
            PopUps editPopUp = new PopUps(patientToBeEdited);
            editPopUp.addOrEditPatient(false);
            ui.updateObservablePatients();
        }catch (IllegalArgumentException e){
            PopUps.errorPopUp(e.getMessage());
            editPatient(ui);
        }

    }

    /**
     * Removes a Patient that is specified through the TableView of the UserInterface from the PatientRegister.
     * If no Patient is chosen, an error message will be shown.
     *
     * @param ui the user interface that wants to perform the method
     * @param register the register of Patients that is to be manipulated
     */
    public void removePatient(UserInterface ui, PatientRegister register){
        Patient patient = ui.getPatientFromTable();
        if(patient == null){
            PopUps.errorPopUp("You did not choose a patient correctly");
            return;
        }
        if(PopUps.deleteConfirmation(patient)){
            register.deletePatient(patient);
            ui.updateObservablePatients();
        }
    }

    /**
     * Opens a FileChooser and lets the user choose where the current PatientRegister will be saved. The only allowed
     * filetype the user can save their register as is .CSV.
     *
     * @param stage the stage which the FileChooser belongs to
     * @param ui the user interface that wants to perform the method
     * @param register the register that is to be saved
     */
    public void writeToCSV(Stage stage, UserInterface ui, PatientRegister register){
        File writeFile = PopUps.chooseCSVFile().showSaveDialog(stage);

        if(writeFile == null){ //If no file is chosen, the writeFile will be null, method will return and nothing happens
            return;
        }

        FileManager.writeToCSV(register.getPatientRegister(), writeFile);
        ui.updateObservablePatients();
    }

    /**
     * Opens a FileChooser and lets the user choose which file they want to read from. The file has to be a .CSV as
     * specified by the chooseVSCFile method.
     *
     * @param stage the stage which the FileChooser belongs to
     * @param ui the user interface that wants to perform the method
     * @param register the register that is to be read from
     */
    public void readFromCSV(Stage stage, UserInterface ui, PatientRegister register){
        File readFile = PopUps.chooseCSVFile().showOpenDialog(stage);
        if(readFile == null){ //If no file is chosen, the writeFile will be null, method will return and nothing happens
            return;
        }

        //Checks if the chosen file actually is a file, and if the chosen file ends with .csv with the private helper method getFileExtension
        if(!readFile.isFile() || !getFileExtension(readFile).equalsIgnoreCase(".csv")){
            PopUps.errorPopUp("Either you didn't chose a file or you chose an unsupported file.\nPlease choose another file.");
            return;
        }

        register.setPatients(FileManager.readFromCSV(readFile));
        ui.updateObservablePatients();
    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "";
        }
        return name.substring(lastIndexOf);
    }
}
