package no.ntnu.edu.idatt2001.williagt.mappe2.model;

import no.ntnu.edu.idatt2001.williagt.mappe2.model.patient.Patient;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class FileManager {

    /**
     * Writes an ArrayList of Patients to a file, and is used for writing to CSV. Uses private helper method
     * patientInfoToStringArray(Patient patient) to split up information about a Patient into parts.
     *
     * This method writes Patient information in this order: firstName, lastName, generalPractitioner, socialSecurityNumber and diagnosis.
     *
     * @param patients the list of patients that are to be saved
     * @param fileName the file that is to be written to
     */
    public static void writeToCSV(ArrayList<Patient> patients, File fileName){
        try (OutputStreamWriter fs = new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8)){

            for (Patient patient : patients) {
                String[] patientInfo = patientInfoToStringArray(patient);
                for (int i = 0; i < patientInfo.length; i++) {
                    fs.append(patientInfo[i]);
                    if(i < (patientInfo.length-1))
                        fs.append(";");
                }
                fs.append(System.lineSeparator());
            }

            fs.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String[] patientInfoToStringArray(Patient patient){
        String[] patientInfo = new String[5];
        patientInfo[0] = patient.getFirstName();
        patientInfo[1] = patient.getLastName();
        patientInfo[2] = patient.getGeneralPractitioner();
        patientInfo[3] = patient.getSocialSecurityNumber();
        patientInfo[4] = patient.getDiagnosis();
        return patientInfo;
    }

    /**
     * Reads information from a file, from CSV in this program's context, converts this into an ArrayList of Patients
     * and returns this list. the Patients are made with the private helper method createPatient(String[] patientInfo).
     *
     * The read method will not count Patients with socialSecurityNumber shorter than 11 characters as real Patients,
     * ang ignores these.
     *
     * The helper method creates Patients with info based on this order: firstName, lastName, generalPractitioner, socialSecurityNumber
     * and diagnosis.
     *
     * @param fileName the file that is to be read
     * @return an ArrayList of Patients
     */
    public static ArrayList<Patient> readFromCSV(File fileName){
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8))){
            ArrayList<Patient> list = new ArrayList<>();
            String line = "";

            while((line = reader.readLine()) != null){
                String[] patientInfo = line.split(";");
                if(patientInfo[3].length() != 11 || !patientInfo[3].matches("^[0-9]*$")){ //If social security number is not 11 digits, this patient won't be considered a real and is skipped
                    continue;
                }
                list.add(createPatient(patientInfo));
            }

            return list;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Patient createPatient(String[] patientInfo){
        String firstName = patientInfo[0];
        String lastName = patientInfo[1];
        String generalPractitioner = patientInfo[2];
        String socialSecurityNumber = patientInfo[3];
        if(patientInfo.length > 4){
            String diagnosis = patientInfo[4];
            return new Patient(firstName, lastName, diagnosis, generalPractitioner, socialSecurityNumber);
        }
        return new Patient(firstName, lastName, generalPractitioner, socialSecurityNumber);
    }
}
