package no.ntnu.edu.idatt2001.williagt.mappe2.model.patient;

import java.util.Objects;

public class Patient {

    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis = " ";
    private String generalPractitioner = " ";

    /**
     * Constructor taking all the information about a Patient: firstName, lastName, diagnosis, generalPractitioner
     * and socialSecurityNumber.
     *
     * firstName, lastName and socialSecurityNumber can't be blank, and socialSecurityNumber must contain exactly 11
     * characters which are digits. If these conditions are not met, an IllegalArgumentException is thrown.
     *
     * @param firstName the Patient's first name
     * @param lastName the Patient's last name
     * @param diagnosis the Patient's diagnosis
     * @param generalPractitioner the Patient's general practitioner
     * @param socialSecurityNumber the Patient's social security number
     */
    public Patient(String firstName, String lastName, String diagnosis, String generalPractitioner, String socialSecurityNumber){
        if(firstName.isBlank() || lastName.isBlank() || socialSecurityNumber.isBlank()){
            throw new IllegalArgumentException("A patient must at least have a first name, last name and social security number");
        }
        if(socialSecurityNumber.trim().length() != 11 || !socialSecurityNumber.matches("^[0-9]*$")){
            throw new IllegalArgumentException("A patient's social security number must contain 11 digits");
        }
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Constructor taking in the bare minimum information of a Patient and its general practitioner: firstName, lastName,
     * generalPractitioner and socialSecurityNumber.
     *
     * firstName, lastName and socialSecurityNumber can't be blank, and socialSecurityNumber must contain exactly 11
     * characters which are digits. If these conditions are not met, an IllegalArgumentException is thrown.
     *
     * @param firstName the Patient's first name
     * @param lastName the Patient's last name
     * @param generalPractitioner the Patient's general practitioner
     * @param socialSecurityNumber the Patient's social security number
     */
    public Patient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber){
        if(firstName.isBlank() || lastName.isBlank() || socialSecurityNumber.isBlank()){
            throw new IllegalArgumentException("A patient must at least have a first name, last name and social security number");
        }
        if(socialSecurityNumber.trim().length() != 11 || !socialSecurityNumber.matches("^[0-9]*$")){
            throw new IllegalArgumentException("A patient's social security number must contain 11 digits");
        }
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Constructor taking in the bare minimum information of a Patient: firstName, lastName and socialSecurityNumber.
     * None of these can be blank, and socialSecurityNumber must contain exactly 11 characters which are digits. If
     * these conditions are not met, an IllegalArgumentException is thrown.
     *
     * @param firstName the Patient's first name
     * @param lastName the Patient's last name
     * @param socialSecurityNumber the Patient's social security number
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber){
        if(firstName.isBlank() || lastName.isBlank() || socialSecurityNumber.isBlank()){
            throw new IllegalArgumentException("A patient must at least have a first name, last name and social security number");
        }
        if(socialSecurityNumber.trim().length() != 11 || !socialSecurityNumber.matches("^[0-9]*$")){
            throw new IllegalArgumentException("A patient's social security number must contain 11 digits");
        }
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /** Generic getters */
    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getDiagnosis(){
        return diagnosis;
    }

    public String getGeneralPractitioner(){
        return generalPractitioner;
    }

    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    }

    /**
     * Sets first name of a Patient. This cannot be empty, and will throw an IllegalArgumentException if this is tried.
     *
     * @param firstName new last name of a Patient
     */
    public void setFirstName(String firstName){
        if(firstName.isBlank()){
            throw new IllegalArgumentException("First name cannot be blank");
        }
        this.firstName = firstName.trim();
    }

    /**
     * Sets last name of a Patient. This cannot be empty, and will throw an IllegalArgumentException if this is tried.
     *
     * @param lastName new last name of a Patient
     */
    public void setLastName(String lastName){
        if(lastName.isBlank()){
            throw new IllegalArgumentException("Last name cannot be blank");
        }
        this.lastName = lastName.trim();
    }

    /**
     * Sets a Patient's diagnosis. This can be blank e.g. if the Patient is cured.
     *
     * @param diagnosis the Patient's diagnosis
     */
    public void setDiagnosis(String diagnosis){
        this.diagnosis = diagnosis;
    }

    /**
     * Sets a Patient's general practitioner field. A Patient may sometimes not have a general practitioner appointed,
     * e.g. if no one are available. This field can therefore be blank.
     *
     * @param generalPractitioner the Patient's general practitioner
     */
    public void setGeneralPractitioner(String generalPractitioner){
        this.generalPractitioner = generalPractitioner;
    }

    //It does not make sense that a social security number can be edited after the fact. This is a unique number that
    //should not be done anything with, like in the real world. This is the reason why this class not has a setter for this.

    /**
     * Returns a boolean based on whether two Patients are equal or not. The equality is decided by the social security number.
     *
     * @param o the object that is compared to another Patient
     * @return true if the object has the same social security number as the Patient, false if not
     */
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    /**
     * Creates and returns a hash code for a Patient.
     *
     * @return a hash code for a Patient
     */
    @Override
    public int hashCode(){
        return Objects.hash(socialSecurityNumber);
    }

    /**
     * Returns a String containing information about a Patient's name and social security number.
     *
     * @return a String containing information about a Patient
     */
    @Override
    public String toString(){
        return firstName + " " + lastName + ", " + socialSecurityNumber;
    }
}
