package no.ntnu.edu.idatt2001.williagt.mappe2.model.patient;

import java.util.ArrayList;

/**
 * Class for manipulating list of Patients.
 */
public class PatientRegister {
    private ArrayList<Patient> patientRegister = new ArrayList<>();

    /**
     * Adds a given patient to the patientRegister if it does not contain such a Patient, and returns boolean based on this.
     * Whether the Patient is added or not is decided by the Patient's social security number.
     * If the Patient already is in the register, an IllegalArgumentException will be thrown.
     *
     * @param patient Patient that is to be added to the register
     * @return true if the patient is added, false if not
     */
    public boolean addPatient(Patient patient){
        if(patient == null){
            return false;
        }
        if(patientRegister.contains(patient)){
            throw new IllegalArgumentException("There already exists a patient with that social security number");
        }
        return patientRegister.add(patient);
    }

    /**
     * Deletes a given Patient from the register and returns a boolean based on if the Patient is removed (true) or not (false).
     *
     * @param patient Patient to be removed
     * @return true if the Patient is removed, false if not
     */
    public boolean deletePatient(Patient patient){
        return patientRegister.remove(patient);
    }

    /**
     * Sets the register to be another ArrayList of Patients.
     *
     * @param newPatients the new list for the register to hold
     */
    public void setPatients(ArrayList<Patient> newPatients){
        patientRegister = newPatients;
    }

    /**
     * Gets the current ArrayList of Patients.
     *
     * @return the current ArrayList of Patients
     */
    public ArrayList<Patient> getPatientRegister() {
        return patientRegister;
    }
}
