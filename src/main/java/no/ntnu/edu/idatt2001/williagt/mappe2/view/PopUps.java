package no.ntnu.edu.idatt2001.williagt.mappe2.view;

import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import no.ntnu.edu.idatt2001.williagt.mappe2.model.patient.Patient;

import java.util.ArrayList;

/**
 * Class managing various pop up windows for the Patient Register
 */
public class PopUps extends Dialog<Patient> {

    private Patient patientToBeEdited;

    /**
     * Constructor used in conjunction with the PatientRegisterController to edit a Patient. The Patient parameter
     * is set as patientToBeEdited and is used in addOrEditPatient method of this class.
     *
     * @param patient patient set to patientToBeEdited and is to be edited
     */
    public PopUps(Patient patient) {
        patientToBeEdited = patient;
    }

    /**
     * Constructor used for showing about-information of the program. Takes in a boolean info. If info is true, the
     * infoPopUp will be shown and display about-information. Else, nothing noteworthy happens.
     *
     * @param info boolean deciding if about-information is showed
     */
    public PopUps(boolean info){
        super();
        if(info){
            infoPopUp();
        }
    }

    /**
     * Constructor used in conjunction with PatientRegisterController to create a Patient.
     */
    public PopUps(){
    }

    /**
     * Displays about Dialog containing information about the Patient Register program.
     */
    public void infoPopUp(){
        setTitle("About");
        setHeaderText("Patient register\n1.0-SNAPSHOT");
        resizableProperty();
        setContentText("Application made by williagt\n\nidatt2001.ntnu.edu.no");
        getDialogPane().getButtonTypes().addAll(ButtonType.OK);
        showAndWait();
    }

    /**
     * Displays an Alert with given errorMessage.
     * @param errorMessage error message to be displayed by popup window
     */
    public static void errorPopUp(String errorMessage){
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setTitle("Error");
        errorAlert.setHeaderText("Something went wrong");
        errorAlert.setContentText(errorMessage);
        errorAlert.showAndWait();
    }

    /**
     * Displays an Alert asking to confirm a deletion of a given Patient. Returns true if OK is pressed and false if not.
     *
     * @param patient Patient that might be deleted
     * @return true if the user presses OK, false if not
     */
    public static boolean deleteConfirmation(Patient patient){
        Alert deleteConfirm = new Alert(Alert.AlertType.CONFIRMATION);
        deleteConfirm.setTitle("Delete confirmation");
        deleteConfirm.setHeaderText("Delete confirmation");
        deleteConfirm.setContentText("Are you sure you want to delete this patient?\n" + patient.toString());
        deleteConfirm.getButtonTypes().removeAll(ButtonType.CANCEL, ButtonType.OK);
        deleteConfirm.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);
        deleteConfirm.showAndWait();
        return deleteConfirm.getResult() == ButtonType.YES;
    }

    /**
     * Method for creating and returning a FileChooser that can only choose CSV-files.
     *
     * @return FileChooser that only allows CSV files to be chosen
     */
    public static FileChooser chooseCSVFile(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files", "*.csv"));
        return fileChooser;
    }

    /**
     * Shows a Dialog pop up and either creates a new Patient or edits an already existing patient. This is decided by
     * the add boolean and if the user closes the Dialog or presses ok.
     *
     * If add is true, add mode is used and the appropriate information is set for a Dialog pop which is then shown. If the user presses OK
     * a Patient is created and returned.
     *
     * If add is false, edit mode is used and the appropriate information is set for a Dialog pop which is then shown.
     * The object variable patientToBeEdited is used as a base, and changes made by the user are reflected on this object.
     * If the user presses OK, the edited Patient is returned.
     *
     * @param add boolean deciding if the method foes in add or edit mode
     * @return either a new or edited Patient. Returns null if cancel is pressed by user
     */
    public Patient addOrEditPatient(boolean add){

        //Setting information about the Dialog based on if it is add or edit mode
        String title;
        String headerText;
        if(add){
            title = "Add patient";
            headerText = "Add a patient.\nNo fields can be empty, and social security number must be 11 digits.";
        }else {
            title = "Edit patient";
            headerText = "Edit a patient.\nThe patient must have a name, and social security number can't be edited.";
        }
        Dialog<ButtonType> addPatientDialog = new Dialog<>();
        addPatientDialog.setTitle(title);
        addPatientDialog.setHeaderText(headerText);
        addPatientDialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);

        //Creating elements that go into the Dialog and the user can interact with
        DialogPane information = new DialogPane();
        GridPane infoInputGrid = new GridPane();

        TextField firstNameField = new TextField();
        firstNameField.setPromptText("First name");

        TextField lastNameFiled = new TextField();
        lastNameFiled.setPromptText("Last name");

        TextField socialSecurityNumberField = new TextField();
        socialSecurityNumberField.setPromptText("Social security number");

        if(!add){
            firstNameField.setText(patientToBeEdited.getFirstName());
            lastNameFiled.setText(patientToBeEdited.getLastName());
            socialSecurityNumberField.setText(patientToBeEdited.getSocialSecurityNumber());
            socialSecurityNumberField.setEditable(false);
            socialSecurityNumberField.setBackground(Background.EMPTY);
        }

        infoInputGrid.add(new Label("First name: "), 1, 1);
        infoInputGrid.add(firstNameField, 2, 1);

        infoInputGrid.add(new Label("Last name: "), 1, 2);
        infoInputGrid.add(lastNameFiled, 2, 2);

        infoInputGrid.add(new Label("Social security number: "), 1, 3);
        infoInputGrid.add(socialSecurityNumberField, 2, 3);

        information.setContent(infoInputGrid);
        addPatientDialog.getDialogPane().setContent(information);

        //Creating array for information about the Patient to be temporarily stored
        ArrayList<String> patientInfo = new ArrayList<>();

        //Adding to OK and cancel button
        addPatientDialog.showAndWait().ifPresent(response -> {

            //OK button
            if(response == ButtonType.OK){
                patientInfo.add(firstNameField.getText());
                patientInfo.add(lastNameFiled.getText());
                patientInfo.add(socialSecurityNumberField.getText());
            }

            //Cancel button
            else if(response == ButtonType.CANCEL){
                addPatientDialog.close();
            }
        });

        //Returns new Patient if in add mode and OK has been pressed
        if(add && !patientInfo.isEmpty()){
            return new Patient(patientInfo.get(0), patientInfo.get(1), patientInfo.get(2));
        }

        //Returns newly edited Patient if in edit mode and OK has been pressed
        if(!add && !patientInfo.isEmpty()){
            patientToBeEdited.setFirstName(patientInfo.get(0));
            patientToBeEdited.setLastName(patientInfo.get(1));
            return patientToBeEdited;
        }

        //Returns null if used presses cancel
        return null;
    }
}
