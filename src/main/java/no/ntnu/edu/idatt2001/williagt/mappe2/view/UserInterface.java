package no.ntnu.edu.idatt2001.williagt.mappe2.view;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import no.ntnu.edu.idatt2001.williagt.mappe2.controller.PatientRegisterController;
import no.ntnu.edu.idatt2001.williagt.mappe2.model.patient.Patient;
import no.ntnu.edu.idatt2001.williagt.mappe2.model.patient.PatientRegister;
import no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.UserInterfaceNodeFactory;
import no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements.Element;

import java.io.FileNotFoundException;

/**
 * The class responsible for displaying the user interface of the Patient Register and it's contents
 */
public class UserInterface extends Application {

    /**
     * Need all these to be object variables because they are essential for the rest of the program
     */
    private Stage mainStage;
    private PatientRegister register = new PatientRegister();
    private PatientRegisterController controller = new PatientRegisterController();

    private TableView<Patient> registerView;
    private ObservableList<Patient> observablePatients;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.mainStage = stage;

        //Uses factory to make TableView. Need this as object variable because it is used elsewhere in the program
        registerView = (TableView<Patient>) UserInterfaceNodeFactory.getInterfaceNodeElement(Element.TABLEVIEW);


        fillTable();
        stage.setTitle("Patient Register");
        stage.setScene(mainScene());
        stage.show();
    }

    /**
     * Returns a new Scene containing a BorderPane as root containing ui elements from the UserInterfaceNodeFactory.
     *
     * @return a new Scene used by the start method's stage
     * @throws FileNotFoundException if images related to toolBar are not found
     */
    public Scene mainScene() throws FileNotFoundException {
        BorderPane root = new BorderPane();

        VBox top = new VBox();
        root.setTop(top);

        //Uses mentioned factory to make a MenuBar
        MenuBar patientRegisterMenuBar = (MenuBar) UserInterfaceNodeFactory.getInterfaceNodeElement(Element.MENUBAR);
        //Giving MenuBar functionality
        setOnActionForUIElements(patientRegisterMenuBar);
        top.getChildren().add(patientRegisterMenuBar);

        //Uses mentioned factory to make a ToolBar
        ToolBar toolBar = (ToolBar) UserInterfaceNodeFactory.getInterfaceNodeElement(Element.TOOLBAR);
        //Giving ToolBar functionality
        setOnActionForUIElements(toolBar);
        top.getChildren().add(toolBar);

        root.setCenter(registerView);

        root.setPrefSize(1000, 600);
        return new Scene(root);
    }

    /**
     * Fills the TableView registerView with an ObservableList observablePatients.
     */
    public void fillTable(){
        observablePatients = FXCollections.observableArrayList(register.getPatientRegister());
        registerView.setItems(observablePatients);
    }

    /**
     * Updates observablePatients so that the registerView is updated based on the Patients
     * that are in the PatientRegister register.
     *
     */
    public void updateObservablePatients(){
        observablePatients.setAll(register.getPatientRegister());
    }

    /**
     * Gets a marked Patient from the registerView. Returns null if no Patient is marked.
     *
     * @return marked Patient
     */
    public Patient getPatientFromTable(){
        return registerView.getSelectionModel().getSelectedItem();
    }

    /**
     * Sets diverse on actions for ui elements with specified ID at specified ui element.
     * This is where every Button and MenuItem from the factory are given a function.
     * If more Buttons and MenuItems are added in factory, this also needs updating.
     *
     * @param uiElement a ui element, like MenuBar, that has buttons or the like that needs to have functions set
     */
    private void setOnActionForUIElements(Control uiElement){

        //If the uiElement is a ToolBar
        if(uiElement instanceof ToolBar){

            ToolBar toolbar = (ToolBar) uiElement;

            //Traverse all nodes in ToolBar
            for(Node node : toolbar.getItems()){
                if(node.getId().equals("addButton")){ //If node has addButton as id
                    node.setOnMouseClicked(mouseEvent -> controller.addPatient(this, register));
                }
                if(node.getId().equals("editButton")){ //If node has editButton as id
                    node.setOnMouseClicked(mouseEvent -> controller.editPatient(this));
                }
                if(node.getId().equals("deleteButton")){ ///If node has deleteButton as id
                    node.setOnMouseClicked(mouseEvent -> controller.removePatient(this, register));
                }
            }
        }

        //If the uiElement is a MenuBar
        if(uiElement instanceof MenuBar){

            MenuBar menuBar = (MenuBar) uiElement;

            //Traverse all elements in MenuBar at lowest level
            for(Menu menu : menuBar.getMenus()){
                for(MenuItem menuItem : menu.getItems()){
                    if(menuItem.getId().equals("Import from .CSV")){ //MenuItem has "Import from .CSV" as id
                        menuItem.setOnAction(importCSV -> controller.readFromCSV(mainStage, this, register));
                    }
                    if(menuItem.getId().equals("Export to .CSV")){ //MenuItem has "Export to .CSV" as id
                        menuItem.setOnAction(exportCSV -> controller.writeToCSV(mainStage, this, register));
                    }
                    if(menuItem.getId().equals("Exit app")){ //MenuItem has "Exit app" as id
                        menuItem.setOnAction(exit -> Platform.exit());
                    }
                    if(menuItem.getId().equals("Add new patient")){ //MenuItem has "Add new patient" as id
                        menuItem.setOnAction(add -> controller.addPatient(this, register));
                    }
                    if(menuItem.getId().equals("Edit selected patient")){ //MenuItem has "Edit selected patient" as id
                        menuItem.setOnAction(edit -> controller.editPatient(this));
                    }
                    if(menuItem.getId().equals("Delete selected patient")){ //MenuItem has "Delete selected patient" as id
                        menuItem.setOnAction(delete -> controller.removePatient(this, register));
                    }
                    if(menuItem.getId().equals("About")){ //MenuItem has "About" as id
                        menuItem.setOnAction(about -> new PopUps(true));
                    }
                }
            }
        }
    }
}
