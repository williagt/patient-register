package no.ntnu.edu.idatt2001.williagt.mappe2.view.factory;

import javafx.scene.Node;
import no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements.*;

import java.io.FileNotFoundException;

/**
 * Node factory class for a clear and easy way to create different GUI elements needed for the Patient Register app.
 */
public class UserInterfaceNodeFactory {

    /**
     * Factory method returning different nodes, or GUI elements, based on an Element enum.
     *
     * @param element enum deciding which GUI element is made
     * @return a Node that is an element of a GUI
     * @throws FileNotFoundException if images in case MENUBAR are not found
     */
    public static Node getInterfaceNodeElement(Element element) throws FileNotFoundException {
        Node interfaceElement = null;
        switch(element){
            case MENUBAR:
                interfaceElement = new MenuBarElement().createElement();
                break;
            case TOOLBAR:
                interfaceElement = new ToolBarElement().createElement();
                break;
            case TABLEVIEW:
                interfaceElement = new TableViewElement().createElement();
                break;
            case STATUSBAR:
                interfaceElement = new StatusBarElement().createElement();
                break;
            default:
                break;
        }
        return interfaceElement;
    }
}
