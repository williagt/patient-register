package no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements;

/**
 * Enum used for choosing which component the factory makes
 */
public enum Element {
    MENUBAR,
    TOOLBAR,
    TABLEVIEW,
    STATUSBAR
}
