package no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements;

import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;


/**
 * Class used by factory to make a MenuBar for Patient Register app
 */
public class MenuBarElement implements UIElement{

    /**
     * Creates a MenuBar containing different Menus which have different MenuItems. Overview of what MenuBar contains:
     * File - "Import from .CSV", "Export to .CSV", "Exit app"
     * Edit - "Add new patient", "Edit selected patient", "Delete selected patient"
     * Help - "About"
     *
     * @return MenuBar with various elements
     */
    @Override
    public Node createElement()  {
        MenuBar menuBar = new MenuBar();

        //Creates fileMenu and its MenuItems
        Menu fileMenu = new Menu("File");

        MenuItem importFromCSVMenuItem = new MenuItem("Import from .CSV");
        importFromCSVMenuItem.setId("Import from .CSV");

        MenuItem exportToCSVMenuItem = new MenuItem("Export to .CSV");
        exportToCSVMenuItem.setId("Export to .CSV");

        SeparatorMenuItem separator = new SeparatorMenuItem();
        separator.setId("Separator");

        MenuItem exitAppMenuItem = new MenuItem("Exit app");
        exitAppMenuItem.setId("Exit app");

        fileMenu.getItems().addAll(importFromCSVMenuItem, exportToCSVMenuItem, separator, exitAppMenuItem);

        //Creating editMenu and its MenuItems
        Menu editMenu = new Menu("Edit");

        MenuItem addNewPatientMenuItem = new MenuItem("Add new patient");
        addNewPatientMenuItem.setId("Add new patient");

        MenuItem editSelectedPatientMenuItem = new MenuItem("Edit selected patient");
        editSelectedPatientMenuItem.setId("Edit selected patient");

        MenuItem deleteSelectedPatientMenuItem = new MenuItem("Delete selected patient");
        deleteSelectedPatientMenuItem.setId("Delete selected patient");

        editMenu.getItems().addAll(addNewPatientMenuItem, editSelectedPatientMenuItem, deleteSelectedPatientMenuItem);

        //Creating helpMenu and its MenuItem
        Menu helpMenu = new Menu("Help");
        helpMenu.setId("Help menu");

        MenuItem aboutMenuItem = new MenuItem("About");
        aboutMenuItem.setId("About");

        helpMenu.getItems().add(aboutMenuItem);

        //Adding every Menu to the menuBar
        menuBar.getMenus().addAll(fileMenu, editMenu, helpMenu);

        return menuBar;
    }
}
