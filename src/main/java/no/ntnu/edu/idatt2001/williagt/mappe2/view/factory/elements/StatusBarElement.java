package no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements;

import javafx.scene.Node;
import javafx.scene.control.Label;

/**
 *  Class used by factory to make a StatusBar for Patient Register app. Currently not in use.
 */
public class StatusBarElement implements UIElement{
    @Override
    public Node createElement() {
        return new Label("Status: ");
    }
}
