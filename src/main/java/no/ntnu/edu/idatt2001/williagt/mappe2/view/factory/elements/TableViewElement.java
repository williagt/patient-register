package no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements;

import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import no.ntnu.edu.idatt2001.williagt.mappe2.model.patient.Patient;

/**
 *  Class used by factory to make a TableView for Patient Register app.
 */
public class TableViewElement implements UIElement {

    /**
     * Creates and returns a TableView with three columns: on for Patients' first name, one for their last name and one
     * for their social security number. This works as the list where Patients are displayed in the Patient Register app.
     *
     * @return TableView containing three columns for displaying Patient info
     */
    @Override
    public Node createElement() {

        //TableView and its columns
        TableView<Patient> registerView = new TableView();
        registerView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY); //To make the columns fill out the entire breadth

        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        TableColumn<Patient, String> socialSecurityNumberColumn = new TableColumn<>("Social security number");
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        registerView.getColumns().addAll(firstNameColumn, lastNameColumn, socialSecurityNumberColumn);

        return registerView;
    }
}
