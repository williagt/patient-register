package no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Class used by factory to make a ToolBar for the Patient Register app.
 */
public class ToolBarElement implements UIElement{

    /**
     *  Method creates a ToolBar with add, delete and edit buttons with icons.
     *
     * @return ToolBar with buttons
     * @throws FileNotFoundException if icons are not found
     */
    @Override
    public Node createElement() throws FileNotFoundException {
        ToolBar topToolBar = new ToolBar();

        String imagesDirectory = "src/main/resources/images/";

        ImageView addImage = new ImageView(new Image(new FileInputStream(imagesDirectory + "plus icon.png"), 30, 30, true, true));
        ImageView editImage = new ImageView(new Image(new FileInputStream(imagesDirectory + "pencil symbol.jpg"), 30, 30, true, true));
        ImageView deleteImage = new ImageView(new Image(new FileInputStream(imagesDirectory + "minus icon.png"), 20, 20, true, true));

        Button addButton = new Button("Add", addImage);
        addButton.setId("addButton");

        Button editButton = new Button("Edit", editImage);
        editButton.setId("editButton");

        Button deleteButton = new Button("Delete", deleteImage);
        deleteButton.setPrefSize(75, 38);
        deleteButton.setId("deleteButton");

        topToolBar.getItems().addAll(addButton, editButton, deleteButton);
        return topToolBar;
}
}
