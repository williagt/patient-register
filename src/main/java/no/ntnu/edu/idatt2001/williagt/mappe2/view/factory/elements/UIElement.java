package no.ntnu.edu.idatt2001.williagt.mappe2.view.factory.elements;

import javafx.scene.Node;

import java.io.FileNotFoundException;

/**
 * Interface for factory.
 */
public interface UIElement {
    Node createElement() throws FileNotFoundException;
}
