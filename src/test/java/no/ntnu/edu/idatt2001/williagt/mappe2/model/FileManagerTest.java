package no.ntnu.edu.idatt2001.williagt.mappe2.model;

import no.ntnu.edu.idatt2001.williagt.mappe2.model.patient.Patient;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


public class FileManagerTest {

    private ArrayList<Patient> testPatients = new ArrayList<>();

    private static File writeFile = new File("src/test/resources/write test contains no patients.csv");

    private File emptyFile = new File("src/test/resources/contains no patients.csv");
    private File readFile4Columns = new File("src/test/resources/read test with patients 4 columns.csv");
    private File readFile4ColumnsUnexpectedOrder = new File("src/test/resources/read test patients 4 columns unexpected.csv");
    private File readFile5Columns = new File("src/test/resources/read test patients with 5 columns.csv");
    private File readFile5ColumnsUnexpectedOrder = new File("src/test/resources/read test patients with 5 columns unexpected.csv");


    @Nested
    public class writeToCSVTests{

        @BeforeEach
        public void init(){
            testPatients.add(new Patient("fn", "ln", "11111111111"));
            testPatients.add(new Patient("fn2", "ln2", "22222222222"));
            testPatients.add(new Patient("fn3", "ln3", "33333333333"));
        }

        @Test
        public void writeInformationFromARegisterToACSVFile() {
            boolean writeFileHasNoContent = writeFile.length() == 0;
            FileManager.writeToCSV(testPatients, writeFile);
            assertTrue(writeFileHasNoContent && writeFile.length() > 0);
        }
    }

    @Nested
    public class readFromCSVTests{

        @Nested
        public class readingFromCSVWithColumnsInExpectedOrder{

            //There are 9 lines in the readFile, but two of them do not match the requirement of the method readFromCSV()
            //because their socialSecurityNumber are not 11 digits.
            @Test
            public void readFromCSVWithFourColumns() {
                ArrayList<Patient> readPatients = FileManager.readFromCSV(readFile4Columns);
                assertEquals(7, readPatients.size());
            }

            @Test
            public void readFromCSVWithFiveColumns(){
                ArrayList<Patient> readPatients = FileManager.readFromCSV(readFile5Columns);
                assertEquals(7, readPatients.size());
            }

            @Test
            public void readFromCSVThatIsEmpty(){
                ArrayList<Patient> readPatients = FileManager.readFromCSV(emptyFile);
                assertEquals(0, readPatients.size());
            }
        }

        @Nested
        public class readingFromCSVWithColumnsInUnexpectedOrder{

            @Test
            public void readFromCSVFileWith4ColumnsWithUnexpectedOrder(){
                ArrayList<Patient> readPatients = FileManager.readFromCSV(readFile4ColumnsUnexpectedOrder);
                assertEquals(0, readPatients.size());
            }

            @Test
            public void readFromCSVFileWith5ColumnsWithUnexpectedOrder(){
                ArrayList<Patient> readPatients = FileManager.readFromCSV(readFile5ColumnsUnexpectedOrder);
                assertEquals(0, readPatients.size());
            }
        }

    }

    //Making sure the writeFile is deleted and thus does not contain anything for next round of test
    @AfterAll
    public static void afterAll() {
        writeFile.delete();
    }
}