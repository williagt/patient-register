package no.ntnu.edu.idatt2001.williagt.mappe2.model.patient;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {

    PatientRegister testRegister = new PatientRegister();
    Patient testPatient = new Patient("fn", "ln", "d", "gp", "11111111111");

    @Nested
    public class addPatient{

        @Test
        public void addPatientThatIsNotInTheRegister() {
            assertTrue(testRegister.addPatient(testPatient));
        }

        @Test
        public void addPatientThatIsInTheRegister() {
            testRegister.addPatient(testPatient);
            assertThrows(IllegalArgumentException.class, () -> testRegister.addPatient(testPatient));
        }

        @Test
        public void addNullToTheRegister(){
            assertFalse(testRegister.addPatient(null));
        }
    }


    @Nested
    public class deletePatient{

        @Test
        public void deletePatientThatIsInRegister(){
            testRegister.addPatient(testPatient);
            assertTrue(testRegister.deletePatient(testPatient));
        }

        @Test
        public void deletePatientThatIsNotInTheRegister() {
            assertFalse(testRegister.deletePatient(testPatient));
        }
    }
}