package no.ntnu.edu.idatt2001.williagt.mappe2.model.patient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    Patient testPatient;

    @BeforeEach
    public void init(){
        testPatient = new Patient("Henry", "Henrysen", "Stomach hurts", "Bruno Mars", "11111111111");
    }

    @Nested
    public class SetterTests{

        @Nested
        public class HappyDays{

            @Test
            public void setFirstName() {
                testPatient.setFirstName("Knut");
                assertEquals("Knut", testPatient.getFirstName());
            }

            @Test
            public void setLastName() {
                testPatient.setLastName("Knutsen");
                assertEquals("Knutsen", testPatient.getLastName());
            }

            @Test
            public void setDiagnosis() {
                testPatient.setDiagnosis("Knee hurts");
                assertEquals("Knee hurts", testPatient.getDiagnosis());
            }

            @Test
            public void setGeneralPractitioner(){
                testPatient.setGeneralPractitioner("Dr. Kari");
                assertEquals("Dr. Kari", testPatient.getGeneralPractitioner());
            }
        }

        @Nested
        public class SomethingDoneWrongWhenSetting{

            @Test
            public void setFirstNameToEmpty() {
                assertThrows(IllegalArgumentException.class, () -> {
                    testPatient.setFirstName("");
                });
            }

            @Test
            public void setLastNameToEmpty() {
                assertThrows(IllegalArgumentException.class, () -> {
                    testPatient.setLastName("");
                });
            }
        }
    }

    @Nested
    public class EqualsTests{

        @Nested
        public class PatientsAreEqual{

            @Test
            public void twoPatientsHaveCompletelyTheSameData(){
                Patient testPatient2 = new Patient("Henry", "Henrysen", "Stomach hurts", "Bruno Mars", "11111111111");
                assertEquals(testPatient, testPatient2);
            }

            @Test
            public void twoPatientsHaveDifferentNamesEtcButSameSocialSecurityNumber() {
                Patient testPatient2 = new Patient("Arne", "Arnesen", "Butterflies in stomach", "Elvis", "11111111111");
                assertEquals(testPatient, testPatient2);
            }
        }

        @Nested
        public class PatientsAreNotEqual{

            @Test
            public void twoPatientsHaveAllTheSameInformationButSocialSecurityNumber(){
                Patient testPatient2 = new Patient("Henry", "Henrysen", "Stomach hurts", "Bruno Mars", "11111111112");
                assertNotEquals(testPatient, testPatient2);
            }

            @Test
            public void twoPatientsHaveCompletelyDifferentData(){
                Patient testPatient2 = new Patient("Arne", "Arnesen", "Butterflies in stomach", "Elvis", "22222222222");
                assertNotEquals(testPatient, testPatient2);
            }
        }
    }
}